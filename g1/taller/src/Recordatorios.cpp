#include <iostream>
#include <string>
#include <list>
#include <map>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
    Fecha(uint m,uint d);
    int dia() const;
    int mes() const;
	void incrementar_dia();
    bool operator<(const Fecha& f) const;
    bool operator==(const Fecha& o) const;


  private:
    //Completar miembros internos
    int dia_;
    int mes_;
};

Fecha::Fecha(uint mes, uint dia) : mes_(mes), dia_(dia) {};

int Fecha::mes() const{
	return mes_;
}
int Fecha::dia() const{
	return dia_;
}

// ostream Fecha

ostream& operator<<(ostream& os, Fecha f) {
	os << f.dia() << "/" << f.mes();
	return os;
}



bool Fecha::operator==(const Fecha& o) const{
    bool igual_dia = dia_ == o.dia();
    bool igual_mes = mes_ == o.mes();
    // Completar igualdad (ej 9)
    return igual_dia && igual_mes;
}


bool Fecha::operator<(const Fecha& f) const{
	bool res;
	if (mes_ == f.mes())
		res = dia_ < f.dia();
	else
		res = dia_ < f.dia();
	return res;
}

void Fecha::incrementar_dia(){
	uint dia = dia_;
	uint mes = mes_;
	uint mesmod = mes + 1;
	uint diasenmes = dias_en_mes(mes_);
	if(dia == diasenmes)
		mes_ = mes + 1;	
	dia_ = (dia % diasenmes)+1;
}
	

// Ejercicio 11, 12

// Clase Horario

class Horario{
	public:
		Horario(uint hora, uint min); 
		int hora(); 
		int min();
		bool operator<(Horario h);
	
	private:
		uint hora_;
		uint min_;
};

Horario::Horario(uint hora, uint minuto) : hora_(hora), min_(minuto) {};

int Horario:: hora(){
	return hora_;
}
int Horario:: min(){
	return min_;
}

bool Horario::operator<(Horario h) {
	bool res;
	if (this->hora() == h.hora())
		res = this->min() < h.min();
	else
		res = this->hora() < h.hora();
	return res;
}

// ostream Horario

ostream& operator<<(ostream& os, Horario h) {
	os << h.hora() << ":" << h.min();
	return os;
}

// Ejercicio 13

// Clase Recordatorio

class Recordatorio{
	public:
		Recordatorio(Fecha f, Horario h, string m); 
		Fecha fecha(); 
		Horario horario();
		string msj();
	private:
		Fecha fecha_;
		Horario horario_;
		string msj_;
};

Recordatorio::Recordatorio(Fecha fecha, Horario horario, string mensaje ) : horario_(horario), fecha_(fecha), msj_(mensaje) {};

Horario Recordatorio:: horario(){
	return horario_;
}
Fecha Recordatorio:: fecha(){
	return fecha_;
}

string Recordatorio:: msj(){
		return msj_;
}

// ostream Recordatorio

ostream& operator<<(ostream& os, Recordatorio r) {
	os << r.msj() << " @ " << r.fecha() << " " << r.horario();
	return os;
}



// Ejercicio 14

// Clase Agenda
class Agenda { 
	public: 
		Agenda(Fecha f); 
		void agregar_recordatorio(Recordatorio rec); 
		void incrementar_dia(); 
		list<Recordatorio> recordatorios_de_hoy(); 
		Fecha hoy();
	private:
		Fecha f_;
		map<Fecha, list<Recordatorio>> recs_;
};


Agenda::Agenda(Fecha fecha_inicial) : f_(fecha_inicial), recs_() {};

void Agenda:: agregar_recordatorio(Recordatorio rec){
	Fecha f = rec.fecha();
	map<Fecha,list<Recordatorio>> recs = recs_;
	list<Recordatorio> recos;
	if(recs.count(f)>0){
		map<Fecha,list<Recordatorio>>::iterator it; 
		it = recs.find(f);
		if(it != recs.end())
			recos = it->second;
	}
	//if (recs.count(f) == 1)
	//	recos = recs[f];
	list<Recordatorio>::iterator pos;
	for(list<Recordatorio>::iterator i=recos.begin(); i!=recos.end(); i++){
		Recordatorio reco = *i;
		Horario recoh = reco.horario();
		Horario rech = rec.horario();
		if(rech < recoh)
			pos=i;
	} 
	recos.insert(pos,rec);
	recs_.insert(pair<Fecha,list<Recordatorio>>(f,recos));
}

list<Recordatorio>  Agenda:: recordatorios_de_hoy(){
	Fecha f = f_;
	list<Recordatorio> recos;
	map<Fecha, list<Recordatorio>> recs = recs_;
	if(recs.count(f)==1){
		map<Fecha,list<Recordatorio>>::iterator it;
		it = recs.find(f);
		if(it != recs.end())
			recos = it->second;
	}
	//if (recs_.count(f) == 1)
	//	recos = recs[f];
	return recos;
}

void Agenda:: incrementar_dia(){
	f_.incrementar_dia();
}

Fecha Agenda:: hoy(){
	return f_;
}


// ostream Agenda

ostream& operator<<(ostream& os, Agenda a) {
	os << a.hoy() << endl; 
	os << "=====" << endl;
	list<Recordatorio> recs = a.recordatorios_de_hoy();
	for(list<Recordatorio>::iterator i=recs.begin(); i!=recs.end(); i++){
		os << *i << endl;
	}
	return os;
}


