#include <bits/stdc++.h> 


template <class T>
Conjunto<T>::Nodo::Nodo(const T& v) :valor(v), izq(nullptr), der(nullptr),padre(nullptr) {}


template <class T>
Conjunto<T>::Conjunto() 
{
	cardinal_= 0;
	_raiz = nullptr;
}

template <class T>
Conjunto<T>::~Conjunto() { 
	//delete(_raiz);
	while(_raiz != nullptr){
		this->remover(maximo());
	}
	if(cardinal_ != 0)
		delete(_raiz);
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
	if (_raiz == nullptr)
		return false;
	else{
		Nodo* iterador = _raiz;
		while (iterador != nullptr){
			if(clave < iterador->valor){
				iterador = iterador->izq;
				}
			else if(clave > iterador->valor){
				iterador = iterador->der;
			}
			else{ 
				return true;
			}
		}
	return false;
	}
}
template <class T>
void Conjunto<T>::insertar(const T& clave) {
	if(!pertenece(clave)){
		// en el caso que la raíz es nula agrego el nuevo nodo y ya
		if(_raiz == nullptr){
			Nodo* nuevo = new Nodo(clave);
			_raiz = nuevo;
			cardinal_++;
			//nuevo=nullptr;
			}
		else{
			Nodo* iterador = _raiz;
			Nodo* padre = nullptr;
			// busco la posicion donde vamos a insertar
			while (iterador != nullptr){
				if(clave > iterador->valor){
					padre = iterador;
					iterador = iterador->der;
				}
				else{
					padre = iterador;
					iterador = iterador->izq;
				}				
			}
			//inserto el nodo nuevo
			if (clave > padre->valor){
				Nodo* nuevo = new Nodo(clave);
				nuevo->padre = padre;
				iterador=nuevo;
				padre->der = nuevo;
				cardinal_++;
				}
			else {
				Nodo* nuevo = new Nodo(clave);
				nuevo->padre = padre;
				iterador=nuevo;
				padre->izq=nuevo;
				cardinal_++;
				}
			}
	}		
}

	



template <class T>
void Conjunto<T>::remover(const T& clave) {
	/* buscamos el nodo con valor clave */
	Nodo* actual = _raiz;
	Nodo* padre = nullptr;
	while (actual != nullptr){
		//padre = actual;
		if(clave > actual->valor){
			actual = actual->der;
		}
		else if(clave < actual->valor){
			actual = actual->izq;
		}
		else{
			break;
		}
	}
	padre = actual->padre;
	/* separamos en casos dependiendo cuantas hojas tenga el nodo */
	if(_raiz == nullptr){
		}
	else{
		if(_raiz->valor == clave && cardinal_==1){
			_raiz =nullptr;
			delete(actual);
			cardinal_ = 0;
			}
		else if(actual->der == nullptr || actual->izq==nullptr){
			if(actual->der == nullptr && actual->izq==nullptr){
				if(_raiz->valor != clave){
					if(padre->der == actual){
						padre->der=nullptr;
						//actual=nullptr;
						delete(actual);
						cardinal_--;
					}else
					{
						padre->izq = nullptr;
						//actual=nullptr;
						delete(actual);
						cardinal_--;
					}
				}
				else{
					_raiz=nullptr;
					cardinal_--;					
				}
			}
			else{
				if(actual->der == nullptr ){
					if(_raiz->valor != clave){
						if(padre->der == actual)
							padre->der = actual->izq;
						else
							padre->izq = actual->izq;
						(actual->izq)->padre = padre;
						delete(actual);
						cardinal_--;
					}
					else{
						_raiz = actual->izq;
						//_raiz->izq = nullptr;
						delete(actual);
						cardinal_--;
					}
				}
				else {
					if(_raiz->valor != clave){
						if(padre->der == actual)
							padre->der = actual->der;
						else
							padre->izq = actual->der;
						(actual->der)->padre = padre;
						delete(actual);
						cardinal_--;
					}
					else{
						_raiz=actual->der;
						//_raiz->der = nullptr;
						delete(actual);
						cardinal_--;
					}
				}
			}
		}	
		else{ 
			//si tiene dos hijos el nodo
			Nodo* temp = actual->der;
			Nodo* p = nullptr;
			while(temp->izq != nullptr){
				temp = temp->izq;
				}
			p = temp->padre;
			T copia = temp->valor;
			this->remover(copia);
			actual->valor = copia;
			//delete(temp);
			//if(temp != nullptr)
				//delete(temp);
		}
	}		
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
	/* buscamos el nodo con valor clave */
	Nodo* iterador = _raiz;
	Nodo* padre = nullptr;
	while (iterador != nullptr){
		padre = iterador->padre;
		if(clave > iterador->valor){
			iterador = iterador->der;
			padre = iterador->padre;
		}
		else if(clave < iterador->valor){
			iterador = iterador->izq;
			padre = iterador->padre;
		}
		else{
			break;
		}
	}
	/* en el caso que tenga hijo a la der busca el menor ahí  */
    if(iterador->der != nullptr){
		Nodo*  min = iterador->der;
		while(min->izq != nullptr){
			min = min->izq;}
		return(min->valor);
	} 
    else{
		/* en el caso que no tenga hijo a la der  */
		Nodo* temp = _raiz;
		Nodo* p = nullptr;
		while (temp != nullptr) { 
			if (clave < temp->valor) { 
				p = temp;
				temp = temp->izq; 
			} 
			else if (clave > temp->valor){ 
				temp = temp->der;
			}
			else
				break;
		}
		if(p!=nullptr)
			return p->valor; 
		else
			return _raiz->valor;
	}	
    
}

template <class T>
const T& Conjunto<T>::minimo() const {
	Nodo*  min = _raiz;
	while(min->izq != nullptr){
		min = min->izq;}
	return(min->valor);
}

template <class T>
const T& Conjunto<T>::maximo() const {
	Nodo* max = _raiz;
	while(max->der != nullptr){
		max = max->der;}
    return(max->valor);
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return cardinal_;    
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

