#include <bits/stdc++.h> 


template <class T>
Conjunto<T>::Nodo::Nodo(const T& v) :valor(v), izq(nullptr), der(nullptr),padre(nullptr) {}


template <class T>
Conjunto<T>::Conjunto() 
{
	cardinal_= 0;
	_raiz = nullptr;
}

template <class T>
Conjunto<T>::~Conjunto() { 
	delete(_raiz);
	/*while(_raiz != nullptr){
		this->remover(maximo());
	}
	if(cardinal_ != 0)
		delete(_raiz);*/
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
	if (_raiz == nullptr)
		return false;
	else{
		Nodo* iterador = _raiz;
		while (iterador != nullptr){
			if(clave < iterador->valor){
				iterador = iterador->izq;
				}
			else if(clave > iterador->valor){
				iterador = iterador->der;
			}
			else{ 
				return true;
			}
		}
	return false;
	}
}



   



template <class T>
void Conjunto<T>::insertar(const T& clave) {
	if(!pertenece(clave)){
		// en el caso que la raíz es nula agrego el nuevo nodo y ya
		if(_raiz == nullptr){
				Nodo* nuevo = new Nodo(clave);
				_raiz = nuevo;
				cardinal_++;
			}
		else{
			cardinal_++;
			Nodo* iterador = _raiz;
			Nodo* padre = nullptr;
			// busco la posicion donde vamos a insertar
			while (iterador != nullptr){
				
				if(clave > iterador->valor){
					padre = iterador;
					iterador = iterador->der;
				}
				else{
					padre = iterador;
					iterador = iterador->izq;
				}				
			}
			//inserto el nodo nuevo
			if (clave > padre->valor){
				Nodo* nuevo = new Nodo(clave);
				nuevo->padre = padre;
				iterador=nuevo;
				padre->der = nuevo;
				}
			else {
				Nodo* nuevo = new Nodo(clave);
				nuevo->padre = padre;
				iterador=nuevo;
				padre->izq=nuevo;
				}
					
			}
	}		
}

	


template <class T>
void Conjunto<T>::remover(const T& clave) {
	
	/* buscamos el nodo con valor clave */
	Nodo* actual = _raiz;
	Nodo* padre = nullptr;
	while (actual != nullptr){
		//padre = actual;
		if(clave > actual->valor){
			actual = actual->der;
		}
		else if(clave < actual->valor){
			actual = actual->izq;
		}
		else{
			break;
		}
		padre = actual->padre;
	}
	/* separamos en casos dependiendo cuantas hojas tenga el nodo */
	if(_raiz == nullptr){
		}
	else{
		if(_raiz->valor == clave){
			_raiz =nullptr;
			cardinal_--;
			}
		else if(actual->der == nullptr || actual->izq==nullptr){
			if(actual->der == nullptr && actual->izq==nullptr){
				/*if(padre==nullptr){
					delete actual;
				}
				else{*/
					if(padre->der == actual){
						padre->der=nullptr;
						actual=nullptr;
						cardinal_--;
					}else
					{
						padre->izq = nullptr;
						actual=nullptr;
						cardinal_--;
					}
					
			}
			else{
				if(actual->der == nullptr ){
					if(padre->der == actual)
						padre->der = actual->izq;
					else
						padre->izq = actual->izq;
					(actual->izq)->padre = padre;
					delete(actual);
					cardinal_--;
					/*
					actual->valor = (actual->izq)->valor;
					actual->izq = nullptr;
					(actual->izq)->padre = nullptr;*/
				}
				else {
					if(padre->der == actual)
						padre->der = actual->der;
					else
						padre->izq = actual->der;
					(actual->der)->padre = padre;
					delete(actual);
					cardinal_--;
					/*
					actual->valor = (actual->der)->valor;
					actual->der = nullptr;*/
				}
			}
		}	
		else{ //si tiene dos hijos el nodo
			Nodo* temp = actual->der;
			Nodo* p = nullptr;
			while(temp->izq != nullptr){
				temp = temp->izq;
				p = temp->padre;
				}
			T copia = temp->valor;
			cardinal_--;
			cardinal_++;
			this->remover(copia);
			actual->valor = copia;
			
			/*
			if(p!=nullptr){
				p->izq = temp->der;
				}
			else{
				actual->der = temp->der;
				}
			delete temp;
			actual->valor = copia;*/
		}
	}		
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
	/* buscamos el nodo con valor clave */
	Nodo* iterador = _raiz;
	Nodo* padre = nullptr;
	while (iterador != nullptr){
		padre = iterador->padre;
		if(clave > iterador->valor){
			iterador = iterador->der;
			padre = iterador->padre;
		}
		else if(clave < iterador->valor){
			iterador = iterador->izq;
			padre = iterador->padre;
		}
		else{
			break;
		}
	}
	/* en el caso que tenga hijo a la der busca el menor ahí  */
    if(iterador->der != nullptr){
		Nodo*  min = iterador->der;
		while(min->izq != nullptr){
			min = min->izq;}
		return(min->valor);
	} 
    else{
		/* en el caso que no tenga hijo a la der  */
		Nodo* temp = _raiz;
		Nodo* p = nullptr;
		while (temp != nullptr) { 
			if (clave < temp->valor) { 
				p = temp;
				temp = temp->izq; 
			} 
			else if (clave > temp->valor){ 
				temp = temp->der;
			}
			else
				break;
		}
		if(p!=nullptr)
			return p->valor; 
		else
			return _raiz->valor;
	}	
    
}

template <class T>
const T& Conjunto<T>::minimo() const {
	Nodo*  min = _raiz;
	while(min->izq != nullptr){
		min = min->izq;}
	return(min->valor);
}

template <class T>
const T& Conjunto<T>::maximo() const {
	//Nodo* nodo = new Nodo(0);
	Nodo* max = _raiz;
	while(max->der != nullptr){
		max = max->der;}
    return(max->valor);
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return cardinal_;    
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

