template <typename T>
string_map<T>::string_map(){
    raiz = nullptr;
    _size = 0;
    _claves = {}; 
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    if(raiz!=nullptr){destruirNodo(this->raiz);}
	_size = 0;
	_claves.clear();
	raiz = nullptr;
	delete(this->raiz);
	for(string str : d._claves){
		T significado = d.at(str);
		this->insert(make_pair(str, significado));
	}
}

template <typename T>
string_map<T>::~string_map(){
	destruirNodo(this->raiz);
	raiz=nullptr;
	_size = 0;
	_claves.clear();
	}

/*
template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}
*/

template <typename T>
void string_map<T>::insert(const pair<string, T>& value_type){
	if(_size > 0){
	}
	else{
		Nodo* nuevo = new Nodo();
		raiz = nuevo;
	}
	Nodo* actual = raiz; 
	string clave = value_type.first;
	T significado = value_type.second;
	int len = (value_type.first).size();
	for (int i = 0; i < len; i++){ 
		int indice = int(clave[i]); 
		if (actual->siguientes[indice] == nullptr){ 
			Nodo* nuevo = new Nodo();
			actual->siguientes[indice] = nuevo; }
  
		actual = actual->siguientes[indice]; 
	} 
	actual->fin = true;
	actual->num = _size;
	delete(actual->definicion);
	actual->definicion = new T(value_type.second);
	_size++;
	_claves.push_back(clave);
	
}


template <typename T>
int string_map<T>::count(const string& clave) const{
    if(!raiz)
		return 0;
	else{
		Nodo* actual = raiz;
		int len = clave.size(); 
		for (int i = 0; i < len; i++){ 
			int indice = int(clave[i]); 
			if (!actual->siguientes[indice]) 
				return false; 
			actual = actual->siguientes[indice]; 
		} 
		return (actual != nullptr && actual->fin); 
	}
} 


template <typename T>
const T& string_map<T>::at(const string& clave) const {
	Nodo* actual = raiz;
	int len = clave.size(); 
	for (int i = 0; i < len; i++){ 
		int indice = int(clave[i]);
		actual = actual->siguientes[indice];
	}
	return *actual->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
   	Nodo* actual = raiz;
	int len = clave.size(); 
	for (int i = 0; i < len; i++){ 
		int indice = int(clave[i]);
		actual = actual->siguientes[indice];
	}
	return *actual->definicion;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
	
	// funcion mas complicadita
	// Busco el nodo que quiero borrar
	Nodo* actual = raiz;
	Nodo* ult = raiz;
	int len = clave.size(); 
	int pos=0;
	for (int i = 0; i < len; i++){ 
		int indice = int(clave[i]);
		actual = actual->siguientes[indice];
		if(actual->fin){
			ult = actual;
			pos = i;
		}
	}
	int nume = actual->num;	
	if(_size == 1){
			actual->fin = false;
			destruirNodo(raiz);
			raiz = nullptr;
			_size = 0;
			_claves.clear();
	}
	else{
		if (actual->fin){
			//delete(actual->definicion); 
			actual->fin = false;
			}
		// Elimino todos los nodos con hijos que no sean palabras
		if (actual->siguientes.empty()) { 
			//Nodo* sig = nullptr; 
			if(ult != actual){
				if(ult != raiz){destruirNodo(ult);}
				ult=nullptr;
				/*
				for(int j=pos; j<len; j++){			
					sig = ult->siguientes[j];
					
					delete(ult->definicion);
					delete(ult);
					ult = sig;
				}
			delete(ult->definicion);
			delete(ult);
			ult=nullptr;
			delete(actual);
			actual = nullptr;*/
			} 
		}
		if(_claves.empty() != true){
			_claves.erase(_claves.begin() + nume);
		}
	_size--;
	}
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    if(this->size() == 0)
		return true;
	else
		return false;
}


template <typename T>
void string_map<T>::destruirNodo(string_map:: Nodo* n){
	if(n!=nullptr){
		for(int i=0; i<256; i++){
			if(n->siguientes[i] != nullptr){
				destruirNodo(n->siguientes[i]);
			}
		}
		if(n->definicion != nullptr){
			delete n->definicion;}
		delete n;
		n=nullptr;
	}

}











