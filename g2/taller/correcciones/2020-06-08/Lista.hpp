#include "Lista.h"
#include <stdlib.h> 

Lista::Nodo::Nodo(const int& elem) : valor(elem), sig(nullptr), ant(nullptr) {}

Lista::Lista(){ 
	_primero = _ultimo = nullptr; 
	long_ = 0;
}


Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
	if(_primero != nullptr){
		Nodo* nodo = _primero;
		int len = long_;
		for(Nat i = 0; i<len; i++){
			Nodo* siguiente = nodo->sig;
			delete(nodo);
			nodo=siguiente;
		}
	}
}

Lista& Lista::operator=(const Lista& aCopiar) {
	
	if(this->longitud()>0){
		int size = this->longitud();
		for(Nat i = 0; i<size; i++){
			this->eliminar(0);
		}	
	}	
	int len = aCopiar.longitud();
	Nodo* nodo = aCopiar._primero;
	for(Nat i = 0; i<len; i++){
		this->agregarAtras(nodo->valor);
		nodo = nodo->sig;
	}
	//delete nodo;
	return *this;
}

void Lista::agregarAdelante(const int& elem) {
	Nodo* nodo = new Nodo(elem);  //creo un nodo con elem como data

	if(_ultimo == nullptr){
		_ultimo = nodo;
		_primero = nodo;
	}
	else{
	_primero->ant = nodo;
	nodo->sig = _primero;
	_primero = nodo;	
	}
	long_ = long_+1; //aumentamos la longitud, esto anda OK
}

void Lista::agregarAtras(const int& elem) {
	Nodo* nodo = new Nodo(elem);
	
	if(_primero == nullptr){
		_primero = nodo;
		_ultimo= nodo;
	}
	else{
		_ultimo->sig = nodo;
		//nodo->sig=nullptr;	
		nodo->ant=_ultimo;
		_ultimo = nodo;
	}
	long_ = long_+1;
	
	//delete nodo;
	
}

void Lista::eliminar(Nat i) {
	//Busco primero al nodo que tiene índice i
	int j = i;
	Nodo* nodo_i = _primero;
	for(int cont=0;cont < j; cont++) 
		nodo_i = nodo_i->sig;
	
	// Elimino al nodo separando en casos
    Nodo* anterior = nodo_i->ant;
    Nodo* posterior= nodo_i->sig;
	
	if(anterior == nullptr){
		_primero=nullptr;
		_primero= posterior;
		if(_primero != nullptr)
			_primero->ant=nullptr;	
	}
	else if (posterior == nullptr){
		_ultimo = nullptr;
		_ultimo = anterior;
		if(_ultimo != nullptr)
			_ultimo->sig = nullptr;
	}
	else{
		anterior->sig = posterior;
		posterior->ant = anterior;
	}

	delete nodo_i;
	long_--;
}

int Lista::longitud() const {
	return long_;
}

const int& Lista::iesimo(Nat i) const {
	int j = i; // pongo este j auxiliar para poder usarlo en el for
    Nodo* actual = _primero; // defino al puntero que voy a usar para recorrer a la lista

	for(int cont=0;cont < j; cont++) //mientras el contador sea menor a i lo aumento y me muevo con el puntero
		actual = actual->sig;

	return(actual->valor);
}

int& Lista::iesimo(Nat i) {
	int j = i; // pongo este j auxiliar para poder usarlo en el for
    Nodo* actual = _primero; // defino al puntero que voy a usar para recorrer a la lista

	for(int cont=0;cont < j; cont++) //mientras el contador sea menor a i lo aumento y me muevo con el puntero
		actual = actual->sig;

	return(actual->valor);
}
void Lista::mostrar(ostream& o) {
    
}
